<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MapMarker;
use Faker\Generator as Faker;

$factory->define(MapMarker::class, function (Faker $faker) {
    return [
        'title' => $faker->streetName,
        'lat' => $faker->randomFloat(5, -90, 90),
        'lng' => $faker->randomFloat(5, -180, 180),
    ];
});
