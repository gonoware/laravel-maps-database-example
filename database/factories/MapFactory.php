<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Map;
use Faker\Generator as Faker;

$factory->define(Map::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'lat' => $faker->randomFloat(5, -90, 90),
        'lng' => $faker->randomFloat(5, -180, 180),
        'zoom' => $faker->numberBetween(3, 6),
    ];
});
