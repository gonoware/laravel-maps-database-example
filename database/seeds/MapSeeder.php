<?php

use App\Map;
use App\MapMarker;
use Illuminate\Database\Seeder;

class MapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Map::class, 1)->create()->each(function (Map $map) {
            $map->markers()->saveMany(factory(MapMarker::class, 300)->create(['map_id' => $map->id]));
        });
    }
}
