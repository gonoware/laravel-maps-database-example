# Laravel Maps Database Example

## Guide
 - clone repository
 - run `composer install`
 - copy `env.example` to `.env` and configure the database
 - run `php artisan key:generate`
 - run `php artisan migrate:fresh --seed` to generate some fake maps and markers
 - run `php artisan serve` and open http://localhost:8000
