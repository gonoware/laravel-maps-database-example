<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel Maps Database Example</title>
        @mapstyles
        <style>
            .gnw-map-service {
                height: 900px;
                height: 100vh;
            }
        </style>
    </head>
    <body style="margin: 0; padding: 0;">
        @map($map)
        @mapscripts
        <script>
            // TMP Fix. Will be added to Laravel Maps soon.
            window.addEventListener('LaravelMaps:MapInitialized', function (event) {
                var service = event.detail.service;
                if (service === 'osm') {
                    var map = event.detail.map;
                    map.dragging.disable();
                    delete map.dragging._draggable;
                    map.options.worldCopyJump = true;
                    map.dragging.enable();
                }
            });
        </script>
    </body>
</html>
