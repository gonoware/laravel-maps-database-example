<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $fillable = [
        'name', 'lat', 'lng', 'zoom'
    ];

    public function markers()
    {
        return $this->hasMany(MapMarker::class);
    }
}
