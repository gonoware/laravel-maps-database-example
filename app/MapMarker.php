<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapMarker extends Model
{
    protected $fillable = [
        'title', 'lat', 'lng', 'url'
    ];

    public function map()
    {
        return $this->belongsTo(Map::class);
    }
}
