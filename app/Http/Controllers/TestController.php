<?php

namespace App\Http\Controllers;

use App\Map;
use App\MapMarker;

class TestController
{
    public function index()
    {
        $map = [
            'lat' => 48.134664,
            'lng' => 11.555220,
            'zoom' => 6,
            'markers' => [],
        ];

        $map = Map::with('markers')->find(1);

        $data = $map->only(['lat', 'lng', 'zoom']);
        /*
         * $data = [
         *      'lat' => 48.134664,
         *      'lng' => 11.555220,
         *      'zoom' => 6,
         * ];
         */
        $data['markers'] = $map->markers->map(function (MapMarker $marker) {
           return $marker->only(['lat', 'lng']);
        });
        /*
         * $data = [
         *      'lat' => 48.134664,
         *      'lng' => 11.555220,
         *      'zoom' => 6,
         *      'markers' => [
         *          [
         *              'lat' => 48.134664,
         *              'lng' => 11.555220,
         *          ],
         *          [
         *              'lat' => 47.134664,
         *              'lng' => 10.555220,
         *          ],
         *          ...
         *      ],
         * ];
         */

        return view('welcome', compact('map'));
    }
}
